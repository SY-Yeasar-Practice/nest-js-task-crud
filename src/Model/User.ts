import {Schema} from "mongoose"


const userSchema =  new Schema ({
    personalInfo : {
        firstName: {
            type: String,
            
        },
        lastName: {
            type: String,
           
        }
    },
    loginInfo: {
        email: {
            type: String,
    
         
        },
        password: {
            type: String,
            
        }
    }
},{
    timestamps: true
})

export default  userSchema