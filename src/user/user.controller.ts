import { Body, Controller, Get, Param, Post, Put, Req, Res } from '@nestjs/common';
import { UserService } from './user.service';
import {
  createUserResponse,
  createUserBodyData,
  loginUserBody,
  UserStruct
} from "./user.dto"
import {
  Request,
  Response
} from "express"

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  //register a new user
  @Post ("/registration")
  async createUserHandler (@Body () body: createUserBodyData, @Req () req:Request, @Res() res:Response ): Promise <void> {
    await this.userService.createUser (body,req,res )
  }

  //login  a existing user 
  @Post ("/login") 
  async loginHandler (@Body () body: loginUserBody, @Req () req: Request, @Res() res: Response): Promise<void> {
    return await this.userService.userLoginController (body, req, res)
  }

  //get all user
  @Get ("/show/all") 
  async getAllUser (@Req () req: Request, @Res() res: Response): Promise <void> {
    console.log(`first`)
    return await this.userService.getAllUser (req, res)
  }

  //get individual by id 
  @Get ("/show/:id") 
  async getIndividual (@Req () req: Request, @Res() res: Response, @Param () params): Promise <void> {
    return await this.userService.getIndividual (req, res, params)
  }

  //update user by id 
  @Put ("/update/:id") 
  async updateUser (@Body () body: UserStruct , @Req () req: Request, @Res() res: Response, @Param () params): Promise <void> {
    return await this.userService.updateUser (body, req, res, params)
  }

}

