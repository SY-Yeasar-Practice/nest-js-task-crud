import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
    UserStruct,
    createUserBodyData,
    createUserResponse,
    loginUserBody,
    updateResponse
} from "./user.dto"
import {
    Model
} from "mongoose"
import  {
    Request,
    Response
} from "express"
import * as bcrypt from "bcrypt"
import * as jwt from "jsonwebtoken"
import {config} from "dotenv"
config ()


const jsonSecurity = process.env.jwtSecurity

@Injectable()
export class UserService {
    constructor(
        @InjectModel ("User") private readonly User:Model <UserStruct> 
    ) {}
    //create a new user 
    async createUser (data: createUserBodyData, req:Request, res: Response): Promise <void> {
        const hashedPassword =  await bcrypt.hash (data.password, 10)//hashed the password 
        if (hashedPassword) { //if password hashing successfully 
            const structure:UserStruct  = { //create the structure for schema
                personalInfo : {
                    firstName : data.firstName,
                    lastName: data.lastName
                },
                loginInfo: {
                    email: data.email,
                    password: hashedPassword
                }
            }
            const createNewUser = new this.User (structure) //create a new instance 
            const saveUser = await createNewUser.save() //save new user 
            if (saveUser) { //if save user successfully then it will happen
               res.json ({
                    message: "User create successfully",
                    status: 201,
                    data: saveUser
               })
            }else {
                res.json ({
                    message: "Failed to save",
                    status: 406
                })
            }
        }else {
            res.json ({
                message: "Password Hasing Problem",
                status: 406
            })
        }
    }

    //login a user 
    async userLoginController (body:loginUserBody, req:Request, res:Response): Promise <void> {
        const checkUser:UserStruct = await this.User.findOne ({ //check that is user available or not
            "loginInfo.email": body.email
        })
        if (checkUser) { //check that if user is available or not
            const {
                password:existingPassword
            }: {
                password: string
            } = checkUser.loginInfo
            const matchPassword:boolean = await bcrypt.compare (body.password, existingPassword) //check is it matched or not 
            if (matchPassword) { //if password matched then it will happen
                const payload = {
                    email: checkUser
                }
                const token:string = await jwt.sign (payload,jsonSecurity)
                if (token) {
                    const options = {
                        expires: new Date(
                            Date.now() + 5 * 24 * 60 * 60 * 1000
                        ),
                        httpOnly: true,
                    };
                    res.cookie ("AUTH", token, options ).status (202).json ({
                        message: "Login Successfully"
                    })
                }else {
                    res.status(406).json ( {
                        message: "Token generation failed"
                    })
                }
            }else {
                res.status(404).json ({
                    message: "Password does not match",
                })
            }
        }else {
            res.status(404).json({
                message: "User not found"
            })
        }
    }

    //get individual by id 
    async getIndividual (req:Request, res:Response, params:{id:string}):Promise <void> {
        const getAllUser: UserStruct = await this.User.findOne ({_id:params.id})
        if (getAllUser) {
            res.status (202).json({
                message: "User found",
                user: getAllUser
            })
        }else {
            res.status (202).json({
                message: "User Not found",
                user: null
            })
        }
    }

    //get al user 
    async getAllUser (req:Request, res:Response):Promise <void> {
        console.log(`first`)
        const getAllUser: UserStruct [] = await this.User.find ()
        if (getAllUser) {
            res.status (202).json({
                message: "User founds",
                user: getAllUser
            })
        }else {
            res.status (202).json({
                message: "Users Not found",
                user: null
            })
        }
    }

    //update user by id
    async updateUser (body: UserStruct,  req:Request, res:Response, params:{id:string}):Promise <void> {
        const updateUser: updateResponse = await this.User.updateOne (
            {
                _id: params.id
            }, //query
            {
                $set: body
            }, //update
            {
                multi: true
            } //option
        )
        if (updateUser.modifiedCount != 0 ) { //if update done then it will happen
            res.status (202).json({
                message: "User Updated"
            })
        }else {
            res.status (202).json({
                message: "Users update failed"
            })
        }
    }

}
