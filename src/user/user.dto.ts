//create body user input data interface 
interface createUserBodyData {
    firstName: string
    lastName: string,
    email: string,
    password: string
}

//login time user input body data interface 
interface loginUserBody {
    email: string,
    password: string
}



//create user response data interface 
interface personalInfo {
    firstName: string,
    lastName: string
}

interface loginInfo{ 
    email: string,
    password: string
}

interface UserStruct {
    personalInfo : personalInfo,
    loginInfo: loginInfo,
    createdAt?: object,
    updatedAt?: object
}

interface createUserResponse  {
    message: string,
    status: number,
    data?: UserStruct | object | number

}

interface updateResponse {
    modifiedCount:number,
    acknowledged:boolean,
    upsertedCount: number,
    matchedCount: number
}

export {
    createUserBodyData,
    createUserResponse,
    UserStruct,
    loginUserBody,
    updateResponse
}