import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {config} from  "dotenv"
import * as express from 'express'
import * as cors from "cors"
import * as cookieParser  from "cookie-parser"
config ();

async function bootstrap() {
  const PORT = process.env.PORT || 8080 //get the port from dot env
  const app = await NestFactory.create(AppModule); //create the nest js app 

  app.use (express.json({
    limit: "250mb"
  }));
  app.use (express.urlencoded({
    extended: true,
    limit: "250mb"
  }));
  app.use (cors())
  app.use (cookieParser ())


  const isRunning = await app.listen(PORT);
  if (isRunning) { //if server is connected then it will happen
    console.log(`Server is running on ${PORT}`)
  }else {
    console.log (`Something is wrong with the server connection`)
  }
}
bootstrap();
