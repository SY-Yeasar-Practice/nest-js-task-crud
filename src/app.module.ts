import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {config} from "dotenv"
import { UserModule } from './user/user.module';
config ()
const MongoUrl = process.env.mongoUrl //get the data from dot env

@Module({
  imports: [
    MongooseModule.forRoot (MongoUrl),
    UserModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
